import calculateTotalPrice from "../../utils/calculateTotalPrice.js";
import createRandomId from "../../utils/createRandomId.js";
import getParentElement from "../../utils/getParentElement.js";
import validate from "../../utils/validate.js";
import locations from "../api/getAdministrativeUnits.js";
import orders from "../api/orders.js";
import amountPicker from "../components/AmountPicker/AmountPicker.js";
import { cartList, changeIndicator } from "../components/CartQuantity/CartQuantity.js";
import toast from "../components/toast/Toast.js";
import { keyLocalStorageItemCart, keyLocalStorageListSP } from "../constant.js";
import { clearLocalStorage, getLocalStorage, setLocalStorage } from "../localStorage.js";
import OrderInfo from "../models/OrderInfo.js";
const tbody = document.querySelector("tbody");
const productList = getLocalStorage(keyLocalStorageListSP);
const totalPriceElement = document.querySelector(".total_price");


changeIndicator(cartList);
getTotalPrice(cartList);
setEmptyState(cartList);
// IIFE
let renderProductList = (cartList)=>{
    let html = "";
    cartList.map((item) =>{
        const CurrentProduct = productList.filter(
            (product) => item.idSP === product.id
        )[0];
       html += `<tr class="item_row" id=${item.idSP}>
                    <td class="cart_item">
                    <img src="${CurrentProduct.imgUrl}" width="100px" alt="${CurrentProduct.productName}" />

                    <div class="info">
                        <h2 class="product_name">${CurrentProduct.productName}</h2>
                        <p class="product_quantity ">Quantity: ${CurrentProduct.quantity}</p>
                    </div>
                    </td>
                    <td>
                        <div class="amount_picker">
                            <div class="increase_btn">+</div>
                            <input class="amount_picker_screen" type="number" min="1" max="10" readonly value=${
                            item.soLuong
                            }>
                            <div class="decrease_btn">-</div>
                         </div>
                    </td>
                    <td class="subtotal">${CurrentProduct.price}</td>
                    <td class="total">${CurrentProduct.price*item.soLuong}</td>
                    <td class="clear_cart">
                    <i class="fa-regular fa-circle-xmark remove_btn" title="Remove item"></i>
                    </td>
                </tr>    
       </tr>`
        tbody.innerHTML = html;
    });
    console.log(cartList);
}
renderProductList(cartList);


export function setEmptyState(list){
    const container = document.querySelector(".cart_container");
    // empty state
    if(list.length === 0){
        container.innerHTML = `
        <div class="empty_state">
            <img src="/assets/img/icons/emty_cart.png" alt="Empty Cart" draggable="false">
            <div class="action_btns">
            <a href="index.html" class="back_btn">
                <i class="fa-solid fa-arrow-left"></i>
                <span>Back to Shopping</span>
            </a>
            </div>    
        </div>
        `;
    }
}

export function getTotalPrice(list){
    const total = calculateTotalPrice(list);
    totalPriceElement.innerText = `Total Price: ${total}`;
}


const increaseBtns = document.querySelectorAll(".increase_btn");
const decreaseBtns = document.querySelectorAll(".decrease_btn");
const removeBtns = document.querySelectorAll(".remove_btn")


increaseBtns.forEach(function(increaseBtn){
   increaseBtn.addEventListener("click", function(e){
    amountPicker(e.target,1);
   })
})

decreaseBtns.forEach(function(decreaseBtn){
    decreaseBtn.addEventListener("click",function(e){
        amountPicker(e.target,-1);
    })
})

removeBtns.forEach(function(removeBtn){
    removeBtn.addEventListener("click",function(e){
        if(confirm("Are you sure to remove this item?")){
            const parentElement = getParentElement(e.target,".item_row");
            parentElement.remove();
            const currentCartList = getLocalStorage(keyLocalStorageItemCart);
            const newCartList = currentCartList.filter((item) => item.idSP !== +parentElement.id)
             // save list to local storage and change cart indicator
            changeIndicator(newCartList);
            // update total price
            getTotalPrice(newCartList);
            // set empty state
            setEmptyState(newCartList);
            toast({
                title: "Thành công!",
                message: "Đã xóa sản phẩm khỏi giỏ hàng!",
                type: "success",
            });
        }
    })
})


window.onload = function modal(){
// Get the modal
    const modal = document.getElementById("myModal");
    const  cancelBtn = document.getElementsByClassName("btn_close")[0];
// Get the button that opens the modal
    const btn = document.getElementsByClassName("buy_btn")[0];

// Get the <span> element that closes the modal
    const span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
    btn.onclick = function() {
    modal.style.display = "block";
    }

// When the user clicks on <span> (x), close the modal
    span.onclick = function() {
     modal.style.display = "none";
    }

// When the user clicks anywhere outside of the modal, close it
// When the user clicks on cancel button, close the modal
    window.onclick = function(event) {
        if (event.target == modal) {
        modal.style.display = "none";
        }else if(event.target == cancelBtn){
        modal.style.display = "none";
        }
    }
}

//on buy
const buyBtn = document.querySelector(".buy_btn");
const modalElement = document.querySelector(".modal");

buyBtn.addEventListener("click", function (e) {
    let errorMessage = "Sản phẩm ";
    const invalidProducts = [];

    cartList.forEach((item) => {
        const processingProduct = productList.filter(
            (product) => item.idSP === product.id
        )[0];

        if (item.soLuong > processingProduct.quantity) {
            invalidProducts.push(processingProduct.productName);
        }
    });

    //out of stock order
    if (invalidProducts.length > 0) {
        errorMessage += invalidProducts.join(", ");

        toast({
            title: "Thất bại!",
            message:
                errorMessage + " vượt quá số lượng trong kho, không thể mua!",
            type: "error",
        });
    } else {
        modalElement.classList.add("active");

        //load province on click buy btn
        loadLocation("/p/", "#province");
        districtElement.disabled = true;
        wardElement.disabled = true;
    }
});


async function loadLocation(path, selector, value = "") {
   //fetchApi
   const data = await locations.getLocation(path);
   console.log("data2",data);
   let filteredData = [];
   const selection = document.querySelector(selector);
   switch (selector) {
    case "#province":
        {
            filteredData = data;
        }
        break;
    case "#district":
        {
            //clean old result
            selection.innerHTML = "";
        filteredData = data.filter(
            (item)=> item.province_code === +value)
        }
          //insert placeholder option
          filteredData.unshift({
            name: "--Chọn Quận / Huyện--",
            code: 0,
        });
        break;
    case "#ward":
        {
            selection.innerHTML = "";
            filteredData = data.filter(
                (item)=> item.district_code === +value)
            };
            filteredData.unshift({
                name: "--Chọn Phường / Xã--",
                code: 0,
            });
        break;
   }
   console.log("filter",filteredData);
   filteredData.map((item) => {
    const option = document.createElement("option");
    option.value = item.code;
    option.innerText = item.name;
    selection.appendChild(option);
    });
return value;
}

const provinceElement = document.querySelector("#province");
const districtElement = document.querySelector("#district");
const wardElement = document.querySelector("#ward");
const addressElement = document.querySelector("#address");

let selectedProvince;
let selectedDistrict;
let selectedWard;

provinceElement.addEventListener("change", (e) => {
    loadLocation("/d/", "#district", +e.target.value);

    selectedProvince = e.target.options[e.target.selectedIndex].innerText;

    districtElement.disabled = false;
    wardElement.disabled = true;
});

districtElement.addEventListener("change", (e) => {
    loadLocation("/w/", "#ward", +e.target.value);

    selectedDistrict = e.target.options[e.target.selectedIndex].innerText;

    wardElement.disabled = false;
});

wardElement.addEventListener("change", (e) => {
    selectedWard = e.target.options[e.target.selectedIndex].innerText;
});

const formElement = document.querySelector("form"); 
const lastnameElement = document.querySelector("#lastname");
const firstnameElement = document.querySelector("#firstname");
const emailElement = document.querySelector("#email");
const telElement = document.querySelector("#tel");
const messageElement = document.querySelector("#message");


//validate inputElement by type and event
validate(lastnameElement, "name", "onblur");
validate(lastnameElement, "name", "oninput");
validate(firstnameElement, "name", "onblur");
validate(firstnameElement, "name", "oninput");
validate(emailElement, "email", "onblur");
validate(emailElement, "email", "oninput");
validate(telElement, "tel", "onblur");
validate(telElement, "tel", "oninput");
validate(provinceElement, "", "onblur");
validate(provinceElement, "", "oninput");
validate(districtElement, "", "onblur");
validate(districtElement, "", "oninput");
validate(wardElement, "", "onblur");
validate(wardElement, "", "oninput");
validate(addressElement, "", "onblur");
validate(addressElement, "", "oninput");


const submitButton = document.querySelector("button[type=submit]");

formElement.onsubmit = async function (e) {
    e.preventDefault();

    const validateList = [];

    validateList.push(validate(lastnameElement, "name", "onsubmit"));
    validateList.push(validate(firstnameElement, "name", "onsubmit"));
    validateList.push(validate(emailElement, "email", "onsubmit"));
    validateList.push(validate(telElement, "tel", "onsubmit"));
    validateList.push(validate(provinceElement, "", "onsubmit"));
    validateList.push(validate(districtElement, "", "onsubmit"));
    validateList.push(validate(wardElement, "", "onsubmit"));
    validateList.push(validate(addressElement, "", "onsubmit"));

    const isValid = validateList.every((item) => item === true);

    if (isValid) {
        submitButton.innerHTML = `<i id="loading-icon" class="fa-solid fa-circle-notch"></i>`;
        submitButton.style = "opacity: 0.8";
        submitButton.disabled = true;

        const customerName = `${lastnameElement.value.trim()} ${firstnameElement.value.trim()}`;

        const customerAddress = `${addressElement.value.trim()}, ${selectedWard}, ${selectedDistrict}, ${selectedProvince}`;

        const orderDate = Date.now();

        const newOrder = new OrderInfo(
            await createRandomId(),
            customerName,
            telElement.value.trim(),
            emailElement.value.trim(),
            customerAddress,
            messageElement.value.trim(),
            orderDate,
            cartList
        );

        //call API
        try {
            await orders.postOrder(newOrder);

            cartList.forEach((cartItem) => {
                productList.forEach((productItem) => {
                    if (cartItem.idSP === productItem.id) {
                        productItem.quantity -= cartItem.soLuong;
                    }
                });
            });

            //change quantity of product list
            setLocalStorage(keyLocalStorageListSP, productList);
            //clear cart
            clearLocalStorage(keyLocalStorageItemCart);
            //redirect to bills page 
            window.location.href = "bills.html";
        } catch (err) {
            toast({
                title: "Lỗi gửi dữ liệu",
                message: err,
                type: "error",
            });
        }
    }
};