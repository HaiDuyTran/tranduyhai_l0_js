import toast from "../components/toast/Toast.js";
import { locationApiRoot } from "../constant.js";

export let getLocation = async (path) => {
    try {
        const response = await fetch(`${locationApiRoot}${path}`);

        const data = await response.json();
        console.log("data",data);
        return data;
    } catch (error) {
        toast({
            title: "Không lấy được dữ liệu địa chỉ",
            message: error.message,
            type: "error",
        });
    }
};

//IIFE
const locations = (function () {
    return {
        getLocation,
    };
})();

export default locations;
