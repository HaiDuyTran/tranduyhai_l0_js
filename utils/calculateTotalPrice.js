import { keyLocalStorageListSP } from "../js/constant.js";
import { getLocalStorage } from "../js/localStorage.js";

const productList = getLocalStorage(keyLocalStorageListSP);
export default function calculateTotalPrice(listItem){
    const totalPrice = listItem.reduce((sum, item) => {
        const currentProduct = productList.filter(
            (product) => item.idSP === product.id
        )[0];
        return (sum += item.soLuong * currentProduct.price);
    }, 0);

    return totalPrice;
}